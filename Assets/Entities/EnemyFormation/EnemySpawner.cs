﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;
	private Text myText;
	public float width = 10f;
	public float height = 5f;
	private bool movingRight = true;
	public float speed = 5f;
	public float spawnDelay = 3f;
	private float max;
	private float min;
	public static int wave = 0;
	
	void Start () {

		wave = 0;
		myText = GameObject.Find ("Health").GetComponent<Text> ();
		float distance = transform.position.z - Camera.main.transform.position.z;	
		Vector3 left = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distance));
		Vector3 right = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance));
		max = right.x;
		min = left.x; 
		
		SpawnUntilFull();
	}
	
	void spawnEnemies() {
		foreach (Transform child in transform) {
			GameObject enemy = Instantiate(enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = child;	
		}
	}
	
	void SpawnUntilFull () {
		Transform freePos = NextFreePosition();
		if (freePos) {
			GameObject enemy = Instantiate(enemyPrefab, freePos.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePos;
		}
		if (NextFreePosition()) {
			Invoke("SpawnUntilFull",spawnDelay);
		}	
	}
	
	public void OnDrawGizmos() {
		Gizmos.DrawWireCube(transform.position,new Vector3(width,height));
	}
	
	void Update () {
		if (movingRight) {
			transform.position += Vector3.right*speed*Time.deltaTime;
		} else {
			transform.position += Vector3.left*speed*Time.deltaTime;
		}
		float rightEdge = transform.position.x + 0.5f*width;
		float leftEdge = transform.position.x - 0.5f*width;
		if (leftEdge <= min) {
			movingRight = true;
		} else if (rightEdge >= max) {
			movingRight = false;
		}
		
		if (AllMembersDead()) {
			Debug.Log ("Empty Formation.");
			wave++;
			Debug.Log("Wave " + wave + "incoming.");
			EnemyBehavior.shotsPerSecond += wave/5;
			PlayerController.IncreaseHealth (50f);
			myText.text = PlayerController.health.ToString();
			SpawnUntilFull();
		}
	}
	
	Transform NextFreePosition () {
		foreach (Transform childPositionGameObject in transform) {
			if (childPositionGameObject.childCount <= 0) {
				Debug.Log("Next free position: " + childPositionGameObject);			
				return childPositionGameObject;
			}
		}
		return null;
	}
	
	bool AllMembersDead() {
		foreach (Transform childPositionGameObject in transform) {
			if (childPositionGameObject.childCount > 0) {
				return false;
			}
		}
		return true;
	}
}
