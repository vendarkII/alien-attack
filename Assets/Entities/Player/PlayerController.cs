﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float padding = 1f;
	public static float health = 250f;
	public float speed = 5f;
	public GameObject laser;
	public float beamSpeed;
	public float fireRate = 0.2f;
	public AudioClip laserShot;
	public float volume = 0.125f;
	public Text myText;
	
	float min;
	float max;
	
	void Start () {
		health = 250f;
		myText = GameObject.Find("Health").GetComponent<Text>();
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distance));
		Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance));
		min = leftmost.x + padding;
		max = rightmost.x - padding;
	}
	
	void Fire () {
		GameObject beam = Instantiate(laser,new Vector3(transform.position.x,transform.position.y+0.75f,transform.position.z),Quaternion.identity) as GameObject;
		beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0,beamSpeed,0);
		if (MusicPlayer.soundOn) {
			AudioSource.PlayClipAtPoint(laserShot,beam.transform.position,volume);	
		}
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			InvokeRepeating("Fire",0.0000001f,fireRate);
		}
		
		if (Input.GetKeyUp(KeyCode.Space)) {
			CancelInvoke("Fire");
		}
		
		if (Input.GetKey(KeyCode.LeftArrow)) {
			transform.position += Vector3.left*speed*Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.RightArrow)) {
			transform.position += Vector3.right*speed*Time.deltaTime;
		}
		
		float newX = Mathf.Clamp(transform.position.x, min, max);
		
		transform.position = new Vector3(newX, transform.position.y, transform.position.z);
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		Projectile missile = collider.gameObject.GetComponent<Projectile>();
		if (missile) {
			Debug.Log("Player was hit.");
			health -= missile.getDamage();
			myText.text = health.ToString();
			missile.Hit();
			if (health <= 0) {
				Die();
			}
			Debug.Log("Hit by projectile.");
		}
	}
	
	void Die() {
		LevelManager man = GameObject.Find("LevelManager").GetComponent<LevelManager>();
		man.LoadLevel("Win");
		Destroy(gameObject);
	}

	public static void IncreaseHealth (float increase) {
		PlayerController.health += increase;
	}
}
