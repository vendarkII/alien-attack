﻿using UnityEngine;
using System.Collections;

public class EnemyBehavior : MonoBehaviour {

	public GameObject projectile;
	public float projectileSpeed = 10f;
	public float health = 150f;
	public static float shotsPerSecond = 0.25f;
	public int scoreValue = 10;
	public AudioClip death;
	public AudioClip laserShot;
	public float volume = 0.125f;
	
	private ScoreKepper scoreKeeper;
	
	void Start() {
		if (EnemySpawner.wave == 0) shotsPerSecond = 0.25f;
		scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKepper>();
	}
	
	void Update () {
		float probability = Time.deltaTime * shotsPerSecond;
		if (Random.value < probability) {
			Fire ();
		}
	}
	
	void Fire () {
		Vector3 startPos = transform.position + new Vector3(0,-1,0);
		GameObject missile = Instantiate(projectile, startPos, new Quaternion(1,0,0,0)) as GameObject;
		missile.GetComponent<Rigidbody2D>().velocity = new Vector3(0,-projectileSpeed,0);
		if (MusicPlayer.soundOn) {
			AudioSource.PlayClipAtPoint(laserShot,startPos,volume);
		}
	}
	
	void OnTriggerEnter2D(Collider2D collider){
		Debug.Log(collider);
		Projectile missile = collider.gameObject.GetComponent<Projectile>();
		if (missile) {
			health -= missile.getDamage();
			missile.Hit();
			if (health <= 0) {
				Die();
			}
			Debug.Log("Hit by projectile.");
		}
	}
	
	void Die() {
		if (MusicPlayer.soundOn) AudioSource.PlayClipAtPoint(death,transform.position,volume);
		scoreKeeper.Score(scoreValue);
		Destroy(gameObject);
	}
	
}
