﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
	static MusicPlayer instance = null;
	
	public AudioClip startClip;
	public AudioClip gameClip;
	public AudioClip endClip;
	public static bool soundOn = true;

	public AudioSource music;
	
	void Start () {
		Debug.Log(name);
		soundOn = true;
		if (instance != null && instance != this) {
			Destroy (gameObject);
			print ("Duplicate music player self-destructing!");
		} else {
			instance = this;
			GameObject.DontDestroyOnLoad(gameObject);
			loadAudioSource();
			if (music) Debug.Log("AudioSource found.");
			music.enabled = true;
			music.clip = startClip;
			music.loop = true;
			music.Play();
		}
		
	}
	
	void OnLevelWasLoaded(int level){
			Debug.Log("MusicPlayer: loaded level " + level);
			music.Stop();
			music.clip = decideClip();
			music.loop = true;
			music.Play();
		
	}

	public void muteUnmute() {
		loadAudioSource();
		Debug.Log("Sound Muted/Unmuted");
		if (soundOn) {
			soundOn = false;
			music.Stop();
		} else {
			soundOn = true;
			music.loop = true;
			music.clip = decideClip();
			music.Play();
		}
	}

	AudioClip decideClip() {
		if ( SceneManager.GetActiveScene().buildIndex == 0) {
				return startClip;
			}
		if ( SceneManager.GetActiveScene().buildIndex == 1) {
				return gameClip;
			}
		if ( SceneManager.GetActiveScene().buildIndex == 2) {
				return endClip;
			}
		else {
			return null;
		}
	}

	void loadAudioSource () {
		music = MusicPlayer.instance.GetComponent<AudioSource>();
	}
}
