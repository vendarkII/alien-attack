﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKepper : MonoBehaviour {
	
	public static int score = 0;
	Text myText;
	
	void Start() {
		myText = GetComponent<Text>();
		Reset();
	}
	
	public void Score(int points) {
		Debug.Log ("Score: " + score);
		score += points;
		myText.text = score.ToString();	
	}
	
	public static void Reset () {
		score = 0;
	}
	
}
